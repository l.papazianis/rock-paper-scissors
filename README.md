# Rock Paper Scissors Game

## Instructions
1. Install node(5.8.0) and npm(3.7.3). Versions are Important!
You can use [nvm](https://github.com/creationix/nvm) (node version manager) to install different versions of node
2. Install gulp globall running: npm install gulp -g
3. Run: npm install
4. Run: gulp dev and navigate to the [game](http://localhost:8000/#/). Use the logo to go back!
5. To run the tests run: gulp test

## Architecture
### Code structure
#### Components
The code follows a componentised structure. Each component is each own angular module and it adheres to its own routes. It has its own tests, styles and views.
#### Application
The application is a single page app that requires all the components needed and their styles. Essentially is the entry point. I am bootstraping the application dynamically. This helps me provide to the app asynchronous data (CHOICES.JSON) before the app has initialised yet.
### Build pipeline
I utilized a build pipeline that I have written previously. It all relies on gulp tasks. To bundle the code it uses browserify and babel as the code is written in es6. I could have used simple npm scripts to achieve the same goal and instead of browserify use webpack, but I wrote this pipeline long time ago and it still serves the purpose well. If it ain't broken don't fix it, I guess...
#### RPS engine
The engine is fairly trivial. Upon instantiation it generates a map of all the combinations.  So each time a result is needed there is no need for control flow style code (if else) just access the combinations map with the selection keys. It works like a state-container holding all the combinations in a tree of choices. Traversing it on runtime is cheap. The generation of it its not efficient as there are two nested loops. Its far from perfect in terms of efficiency but it is quite DRY

Someone can extend the game adding lizard and spock (big bang theory) by adding these two extra objects in the mocks/api/choices.json
```javascript
{
    "name":"LIZARD",
    "klass":"fa fa-hand-lizard-o",
    "icon":"",
    "id": "lizard"
},
{
    "name":"SPOCK",
    "klass":"fa fa-hand-spock-o",
    "icon":"",
    "id": "spock"
}
```

## Methodology
### MVP
Initially formed a tiny MVP, broke down the epic into small user stories and subtasks. Created the story board on paper and then fleshed out the wireframes in html. I tried to make a bit responsive without using media queries just having a simple UI that scales relative to the size of the screen. Tried to estimate them and prioritize them and ruled out any overkills (Continues delivery, leaderboard) that wouldn't be able to be finished within a week.
### CI
Used gitlab as it provides ci-runners and with a minimal setup I could achieve continues integration. All the merge requests were merged only if they were passing the build. Passing the build === Pass unit tests, meet code style and linting standards via jshint and jscs. There three 4 CI jobs (build, code_style, static_analysis, test) that are represented into three steps(build, lint, test).[Example pipeline](https://gitlab.com/l.papazianis/rock-paper-scissors/pipelines/5120112)
### Branching strategy
Used a light gitflow. Just develop and feature branches.
### Agile
Created a trello agile board and populated it with my stories. Timeboxed all boilerplate tasks to 3 hours so I don't lose motivation just setting up the project.
### TDD
I didnt follow TDD everywhere, I did only in the game engine. This is were I needed the most as I could validate my results. The coverage is not great, but the test harness, tests the most important things without becoming impeding when refactoring or adding new features.
The gamePage component kept changing all the time in terms of the business logic and the structure and found TDD quite impeding as changing the tests was required every time I had to experiment. I guess TDD is not for POC's :-)