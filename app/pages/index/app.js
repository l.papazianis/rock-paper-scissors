'use strict';

import angular from 'angular';
import landingPage from '../../components/landingPage';
import gamePage from '../../components/gamePage';
angular.module('rock-paper-scissors', [
    landingPage,
    gamePage
]);