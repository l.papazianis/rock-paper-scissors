'use strict';

(function(angular) {
    var application = angular.module('rock-paper-scissors');
    function fetchChoices() {
        var initInjector = angular.injector(['ng']);
        var $http = initInjector.get('$http');
        return $http.get('/mocks/api/choices.json')
            .then(function(res) {
                application.constant('CHOICES', res.data);
            }, function() {
                console.log('Couldnt find the choices object!');
            });
    }
    function bootstrapApplication() {
        angular.element(document).ready(function() {
            angular.bootstrap(document, ['rock-paper-scissors']);
        });
    }
    fetchChoices().then(bootstrapApplication);
}(window.angular));