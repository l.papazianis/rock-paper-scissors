'use strict';
export default {
    'ROCK': {
        'ROCK': 'TIE',
        'PAPER': 'LOST',
        'SCISSORS': 'WON'
    },
    'PAPER': {
        'PAPER': 'TIE',
        'ROCK': 'WON',
        'SCISSORS': 'LOST'
    },
    'SCISSORS': {
        'SCISSORS': 'TIE',
        'ROCK': 'LOST',
        'PAPER': 'WON'
    }
};