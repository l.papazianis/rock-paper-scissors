'use strict';

export default [
    {
        'name':'ROCK',
        'class':'rock-fa',
        'icon':''
    },
    {
        'name':'PAPER',
        'class': 'paper-fa',
        'icon': ''
    },
    {
        'name':'SCISSORS',
        'class':'scissors-fa',
        'icon':''
    }
];