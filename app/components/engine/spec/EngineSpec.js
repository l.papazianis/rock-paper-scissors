'use strict';

import '../index.js';
import 'angular-mocks';
import choices from './choices';
import expectedCombinations from './combinations';
describe('engine', () => {
    var RPS;
    var Engine;
    var generateExpectedResult = (s1, s2) => ({
        player1: {
            selection: s1,
            result: expectedCombinations[s1][s2]
        },
        player2: {
            selection: s2,
            result: expectedCombinations[s2][s1]
        }
    });
    describe('Engine', () => {
        beforeEach(angular.mock.module('rps-engine'));
        beforeEach(angular.mock.inject((
            _RPS_
        ) => {
            RPS = _RPS_;
            Engine = new RPS(choices);
        }));
        it('Should exist', () => expect(RPS).toBeDefined());
        describe('play method', () => {
            it('Should exist', () => expect(Engine.play).toBeDefined());
            describe('When the player 1 selects ROCK and the player 2 SELECTS PAPER', () =>
                it('The winner should be player 2', () =>
                    expect(Engine.play('ROCK', 'PAPER')).toEqual(generateExpectedResult('ROCK', 'PAPER'))));
            describe('When the player 1 selects PAPER and the player 2 SELECTS ROCK', () =>
                it('The winner should be player 1', () =>
                    expect(Engine.play('PAPER', 'ROCK')).toEqual(generateExpectedResult('PAPER', 'ROCK'))));
            describe('When the player 1 selects SCISSORS and the player 2 SELECTS ROCK', () =>
                it('The winner should be player 2', () =>
                    expect(Engine.play('SCISSORS', 'ROCK')).toEqual(generateExpectedResult('SCISSORS', 'ROCK'))));
        });
    });
});