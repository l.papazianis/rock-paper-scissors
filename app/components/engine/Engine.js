'use strict';
export default () => {
    'ngInject';
    return class Engine {
        constructor(choices) {
            this.choices = choices;
            this._generateCombinations();
        }
        play(selection1, selection2) {
            return this._getResults(selection1, selection2);
        }
        _generateCombinations() {
            this.COMBINATIONS = this.choices
                .map(choice => choice.name)
                .reduce((acc, choice, index, choices) => {
                    acc[choice] = {};
                    choices
                        .forEach(oposition => {
                            var opositionIndex = choices.indexOf(oposition);
                            if (index === opositionIndex) {
                                acc[choice][oposition] = 'TIE';
                            } else if (index === choices.length - 1 && opositionIndex === 0) {
                                acc[choice][oposition] = 'WON';
                            } else if (opositionIndex === choices.length - 1 && index === 0) {
                                acc[choice][oposition] = 'LOST';
                            } else {
                                if (index > opositionIndex) {
                                    acc[choice][oposition] = 'LOST';
                                } else {
                                    acc[choice][oposition] = 'WON';
                                }
                            }
                        });
                    return acc;
                }, {});
        }
        _getResults(s1, s2) {
            return {
                player1: {
                    selection: s1,
                    result: this.COMBINATIONS[s2][s1]
                },
                player2: {
                    selection: s2,
                    result: this.COMBINATIONS[s1][s2]
                }
            };
        }
    };
};
