'use strict';

import angular from 'angular';
const componentName = 'rps-engine';
export default componentName;
import RPS from './Engine';
angular
    .module(componentName, [])
    .factory('RPS', RPS);