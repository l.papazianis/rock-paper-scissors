'use strict';
import GameController from './GameController';
export default [
    {
        name: 'layout.gamePage',
        url: '/play/:mode',
        views: {
            'container': {
                templateUrl: 'gamePage/views/game-page.html',
                controller: GameController,
                controllerAs: '$gameCtrl'
            }
        }
    }
];