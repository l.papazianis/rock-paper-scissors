'use strict';
import states from './states';
export default ($stateProvider) => {
    'ngInject';
    states.forEach(state => $stateProvider.state(state));
};