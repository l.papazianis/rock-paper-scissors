'use strict';
export default [
    {
        "name":"ROCK",
        "klass":"fa fa-hand-rock-o",
        "icon":"",
        "id": "rock"
    },
    {
        "name":"PAPER",
        "klass": "fa fa-hand-paper-o",
        "icon": "",
        "id": "paper"
    },
    {
        "name":"SCISSORS",
        "klass":"fa fa-hand-scissors-o",
        "icon":"",
        "id": "scissors"
    }
]