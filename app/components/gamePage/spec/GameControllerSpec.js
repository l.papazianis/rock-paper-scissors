'use strict';

import '../index.js';
import 'angular-mocks';
import CHOICES from './choices';
describe('gamePage', () => {
    var $controller;
    var RPS;
    var $timeout;
    var $q;
    describe('GameController', () => {
        const newCtrlInstance = (params = { mode: 'player-vs-computer'}, choices = CHOICES) =>
            $controller('GameController', {
                RPS,
                $stateParams: params,
                CHOICES: choices,
                $q,
                $timeout
            });
        beforeEach(angular.mock.module('rps-game'));
        beforeEach(angular.mock.module('rps-engine'));
        beforeEach(angular.mock.module('ui.router'));
        beforeEach(angular.mock.inject((
            _$controller_,
            _RPS_,
            _$timeout_,
            _$q_
        ) => {
            $controller = _$controller_;
            RPS = _RPS_;
            $q = _$q_;
            $timeout = _$timeout_;
        }));
        it('Should exist', () => expect(newCtrlInstance()).toBeDefined());
        describe('play method', () => {
            it('Should exist', () => expect(newCtrlInstance().play).toBeDefined());
            it('Should return a promise', () => expect(newCtrlInstance().play().then).toBeDefined());

        });
        describe('reset method', () => {
            it('Should exist', () => expect(newCtrlInstance().reset).toBeDefined());
        });
    });
});