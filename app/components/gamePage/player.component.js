'use strict';
class PlayerComponentController {
    isSelected(index) {
        return this.player.getSelection() === this.choices[index].name;
    }
    select(selection) {
        this.player.setSelection(this.choices[selection].name);
    }
}
export default {
    bindings: {
        player: '=',
        results: '=',
        withSelections: '=?',
        mode: '=',
        choices: '=',
        loading: '='
    },
    controller: PlayerComponentController,
    templateUrl: 'gamePage/views/player-component.html'
};