'use strict';

import Player from './Player';
export default class GameController {
    constructor(RPS, $stateParams, CHOICES, $timeout, $q) {
        'ngInject';
        this.mode = $stateParams.mode;
        this.rpc = new RPS(CHOICES);
        this.player1 = new Player();
        this.player2 = new Player();
        this.choices = CHOICES;
        this.reset();
        this.$timeout = $timeout;
        this.$q = $q;
    }
    play() {
        const process = () => {
            this._setRandomSelections();
            this.results = this.rpc.play(this.player1.getSelection(), this.player2.getSelection());
            this._setSelectionClassOnPlayer('player1');
            this._setSelectionClassOnPlayer('player2');
        };
        return this._loading()
            .finally(process);
    }
    _setRandomSelections() {
        switch (this.mode) {
            case 'computer-vs-computer':
                this._setRandomSelectionToPlayer(this.player1);
                this._setRandomSelectionToPlayer(this.player2);
            break;
            case 'player-vs-computer':
                this._setRandomSelectionToPlayer(this.player2);
            break;
        }
    }
    _setSelectionClassOnPlayer(player) {
        var klass = this.choices.filter(choice => choice.name === this.results[player].selection)[0].klass;
        this[player].setSelectionClass(klass);
    }
    _setRandomSelectionToPlayer(player) {
        var random = Math.floor(Math.random() * ((this.choices.length - 1) - 0 + 1));
        player.setSelection(this.choices[random].name);
    }
    _loading() {
        var dfd = this.$q.defer();
        this.loading = true;
        const turnOfLoading = () => {
            this.loading = false;
            dfd.resolve();
        };
        this.$timeout(turnOfLoading, 2000);
        return dfd.promise;
    }
    reset() {
        this.player1.setSelectionClass(this.choices[0].klass);
        this.player2.setSelectionClass(this.choices[0].klass);
        this.player1.setSelection(null);
        this.player2.setSelection(null);
        this.results = null;
    }
}