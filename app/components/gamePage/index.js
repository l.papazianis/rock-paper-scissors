'use strict';

import angular from 'angular';
import 'ui-router';
import Engine from '../engine/';
import config from './config';
import player from './player.component';
import GameController from './GameController';
const componentName = 'rps-game';
export default componentName;
angular
    .module(componentName, [
        Engine,
        require('./templates').name,
        'ui.router'
    ])
    .component('player', player)
    .controller('GameController', GameController)
    .config(config);