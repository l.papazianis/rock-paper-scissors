'ngInject';

export default class Player {
    setSelection(selection) {
        this.selection = selection;
    }
    getSelection() {
        return this.selection;
    }
    setSelectionClass(klass) {
        this.selectionClass = klass;
    }
}