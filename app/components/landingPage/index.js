'use strict';

import angular from 'angular';
import 'ui-router';
import layout from '../layout';
import config from './config';
const componentName = 'landing-page';
export default componentName;

angular
    .module(componentName, [
        'ui.router',
        layout,
        require('./templates.js').name
    ])
    .config(config);
