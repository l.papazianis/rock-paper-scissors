'use strict';
export default [
    {
        name: 'layout.landingPage',
        url: '/',
        views: {
            'container': {
                templateUrl: 'landingPage/views/landing-page.html'
            }
        }
    }
];