'use strict';
const layoutState = {
    name: 'layout',
    templateUrl: 'layout/views/root.html',
    abstract: true
};
export {layoutState};