'use strict';

import angular from 'angular';
import 'ui-router';
import config from './config';
const componentName = 'layout';
export default componentName;

angular
    .module(componentName, [
        'ui.router',
        require('./templates').name
    ])
    .config(config);
