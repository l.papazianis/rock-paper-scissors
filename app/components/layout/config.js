'use strict';

import {layoutState} from './states.js';
export default ($stateProvider) => {
    'ngInject';
    $stateProvider.state(layoutState);
};